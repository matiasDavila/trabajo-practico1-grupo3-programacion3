package visual;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controles.coordinador;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class ventanaPrincipal extends JFrame {

	private JPanel contentPane;
	private coordinador miCoordinador;
	public JPanel panel;
	private JLabel lblSetlevel;
	public JSpinner niveles;
	public static JTextField txtjugadas;
	private JLabel lblNJugadas;
	public JButton btnjugar;
	


	public ventanaPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.setBounds(31, 64, 372, 151);
		contentPane.add(panel);
		
		
		
	    btnjugar = new JButton("jugar");// y aca comienza la magia evento principal que desencadena el juego
		btnjugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			 miCoordinador.empezarJuego((Integer) niveles.getValue());
				
				
			}
		});
		
	
		btnjugar.setBounds(156, 20, 89, 23);
		contentPane.add(btnjugar);
		
		lblSetlevel = new JLabel("SET-LEVEL");
		lblSetlevel.setBounds(34, 24, 70, 14);
		contentPane.add(lblSetlevel);
		
	    niveles = new JSpinner();
		niveles.setModel(new SpinnerNumberModel(1, 1, 3, 1));
		niveles.setBounds(93, 21, 29, 20);
		niveles.setEnabled(true);
		
		
		contentPane.add(niveles);
		txtjugadas = new JTextField();
		txtjugadas.setBounds(305, 33, 86, 20);
		contentPane.add(txtjugadas);
		txtjugadas.setColumns(10);
		txtjugadas.setEnabled(false);
		
		
		lblNJugadas = new JLabel("N\u00BA jugadas");
		lblNJugadas.setBounds(317, 11, 70, 14);
		contentPane.add(lblNJugadas);

	}

	public void setCoordinador(coordinador miCoordinador) {
		this.miCoordinador=miCoordinador;
		
	}
}
