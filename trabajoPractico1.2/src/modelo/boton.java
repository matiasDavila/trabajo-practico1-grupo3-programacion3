package modelo;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

import controles.coordinador;
import modelo.logica;
import java.awt.Button;

public class boton extends JButton  implements ActionListener {//creamos nuestro boton y le agregamos los atributos que necesitamos( cordenadas estado(color(2)) y coordinador
	
	        private coordinador miCoordinador;
            private boolean estado;
			int x;
			int y;
			
			
			public boton() { //constructor le da un estado x defaul gris, y le agregamos una escucha a evento.
		        super(" "); 
		        
				estado=false;
				setBackground(Color.gray);
			    addActionListener(this);
				
		}
			public boolean cambioEstado() {      // nos cambia el color.
				if(estado==false) {
					estado=true;
					setBackground(Color.orange);
					return estado;
					}
				else estado=false;
				setBackground(Color.gray);
				return estado;
			}
			
		public void cambiarNombre(int x, int y) { //es mas referencial cambia el nombre con que apararecen en el panel
              setText((x+1)+"-"+(y+1));
			}

		@Override
		public void actionPerformed(ActionEvent e) {// es el evento asociado al click del boton.
           
			
			miCoordinador.cambiarEstado(this.x, this.y);
			
			
		}
		public void setCoordinador(coordinador miCoordinador) {
			this.miCoordinador=miCoordinador;
			
		}public boolean getEstado() { // nos da el estado..(true o false)
			return estado;
		}


}


