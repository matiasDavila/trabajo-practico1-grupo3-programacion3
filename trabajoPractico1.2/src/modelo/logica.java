package modelo;

import java.util.Random;

import javax.swing.JButton;
import javax.swing.JTextField;

import controles.coordinador;


public class logica {

	private coordinador miCoordinador;
	private int jugadas=0;
	

	public void setCoordinador(coordinador miCoordinador) {//establece la relacion con el cordinador
		this.miCoordinador=miCoordinador;
		
		
	}

	public boton[][] crearBotones(int nivel) {// llama a crear una matriz de botones de acuerdo al nivel es su tama�o.
		
		if(nivel==1)return matrizBotones(3,3);
		if(nivel==2)return matrizBotones(4,4);
		else return matrizBotones(5,5);
		
	}
	public boton[][] matrizBotones(int filas, int columnas) {  //crea la matriz seteada en "crear botones"
		boton[][] luces=new boton[filas][columnas];
		for(int ancho=0; ancho<columnas;ancho++) {
			for(int alto=0;alto<filas;alto++ ) {
				this.agregarBoton(luces,alto, ancho);
				
				
			}
		}
		return luces;
	}
	public void agregarBoton(boton[][] luces,int fila,int columna) { //agrega un boton a la matriz de estado aleatorio.(color)
		luces[fila][columna]=new boton();
		luces[fila][columna].x=fila;
		luces[fila][columna].y=columna;
		luces[fila][columna].cambiarNombre(fila, columna);
		if(this.estadoAleatorio())luces[fila][columna].cambioEstado();
		
		
		
	}
	private boolean estadoAleatorio() {  //random de estado(color)
		Random aleatorio=new Random();
		int numero=aleatorio.nextInt(10);
		if(numero<4)return false;
		else
           return true;
	}

	public boolean hayBoton(boton[][] botones, int fila, int columna) {  // checa que alla un boton en una cordenada de la matriz.
		if(fila>botones[0].length-1 || fila<0)return false;
		if(columna>botones.length-1 || columna<0)return false;
		else
		
		return true;
	}

	public void cambiarEstados(boton[][] botones, int fila, int columna) {//si hay boton alrededor del boton pulsado lo cambia de estado(color)
		miCoordinador.sumarJugada();
		botones[fila][columna].cambioEstado();
		
		if(hayBoton(botones,fila+1,columna)) {
		botones[fila+1][columna].cambioEstado();}
		
		if(hayBoton(botones,fila-1,columna)) {
		botones[fila-1][columna].cambioEstado();}
		
		if(hayBoton(botones,fila,columna+1)) {
		botones[fila][columna+1].cambioEstado();}
		
		if(hayBoton(botones,fila,columna-1)) {
		botones[fila][columna-1].cambioEstado();}
		
		if(ganaste(botones))finJuego();
		
	}

	public String sumarJugada() {//suma a las jugadas realizadas una mas
		jugadas=jugadas+1;
		return " "+jugadas;
	}

	public boolean ganaste(boton[][] botones) {// checa el estado de todos los botones si todods estan encendidos "ganaste. 
		boolean ret=true;
		for(int e=0;e<botones.length;e++) {
			for(int j=0; j<botones[0].length;j++) {
				ret=ret && botones[e][j].getEstado();
			}
	}
	
	return ret;
	
	}
	public void finJuego() {
		miCoordinador.finDelJuego();  
	}
	
}
