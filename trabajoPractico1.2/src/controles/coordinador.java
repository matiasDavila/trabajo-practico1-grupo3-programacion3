package controles;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import modelo.boton;
import modelo.logica;
import visual.ventanaPrincipal;

public class coordinador {

	private ventanaPrincipal miVentana;
	private static logica miLogica;
	private static boton[][] botones;
	private boton miBoton;

	public void setVentanaPrincipal(ventanaPrincipal miVentana) {
		this.miVentana=miVentana;
		
	}

	public void setLogica(logica miLogica) {
		this.miLogica=miLogica;
		
	}

	public void empezarJuego(Integer nivel) {
		miVentana.panel.removeAll();             //limpiamos el panel (aseguramos que siempre empezara el juego sin residuos del nivel anterior)
		botones=miLogica.crearBotones(nivel);
		miVentana.setTitle("");
		if(nivel==1)miVentana.panel.setBounds(57, 79, 200, 200);   //cambiamos la superficie del panel deacuerdo a la cantidad de botones que tenga el nivel.
		if(nivel==2)miVentana.panel.setBounds(57, 79, 250, 250);
		if(nivel==3)miVentana.panel.setBounds(57, 79, 290, 290);
		for(int e=0;e<botones.length;e++) {
			for(int j=0; j<botones[0].length;j++) {
				
				miVentana.panel.add(botones[e][j]);
             }
		}
		miVentana.panel.setVisible(false);  // de esta manera actualizo el tablero para ver los botones.(rebuscado)
		miVentana.panel.setVisible(true);
		miVentana.btnjugar.setEnabled(false);
	}

	public static void cambiarEstado(int x, int y) {//de acuerdo a la cordenada del boton pulzado cambia el color a los botones corespodientes)
		miLogica.cambiarEstados(botones, x, y);
		
	}	
		

	public void sumarJugada() {
		
		miVentana.txtjugadas.setText(miLogica.sumarJugada());// por cada pulzada de boton suma una jugada.
	}

	public void finDelJuego() {
		
		miVentana.btnjugar.setEnabled(true);//empieza nuevamente el juego.
		miVentana.niveles.setValue(2);
		miVentana.setTitle("CONGRATULATIONS YOU ARE THE WINNER");
		JOptionPane.showMessageDialog(null,"CONGRATULATIONS YOU ARE THE WINNER");
		
	}




		
	



}
