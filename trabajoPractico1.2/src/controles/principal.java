package controles;


import modelo.boton;
import modelo.logica;
import visual.ventanaPrincipal;

public class principal {

	public static void main(String[] args) {
		 //declaramos las clases
		ventanaPrincipal miVentana;
		logica miLogica;
		coordinador miCoordinador;
		boton miBoton;
		
		//instanciamos las clases
		miVentana=new ventanaPrincipal();
		miVentana.setVisible(true);
		miLogica=new logica();
		miCoordinador=new coordinador();
	 	miBoton=new boton();
		
		//establecemos las relaciones;
		miVentana.setCoordinador(miCoordinador);
		miLogica.setCoordinador(miCoordinador);
     	miBoton.setCoordinador(miCoordinador);
		 
		//enviamos las instancias al coordinador
		miCoordinador.setVentanaPrincipal(miVentana);
		miCoordinador.setLogica(miLogica);
	  //  miCoordinador.setBoton(miBoton);
		
		
	}

}
